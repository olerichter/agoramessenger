/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.agoramessenger;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.agoramessenger.bluetooth.BluetoothChatService;
import com.example.android.agoramessenger.protocol.ProtocolHandler;
import com.example.android.agoramessenger.protocol.crypto.OpenPgpHandler;
import com.example.android.agoramessenger.protocol.database.Contact;
import com.example.android.agoramessenger.protocol.database.PersonalMessage;
import com.example.android.agoramessenger.protocol.database.RealmController;
import com.example.android.common.logger.Log;

import java.util.List;

/**
 * This fragment controls Bluetooth to communicate with other devices.
 */
public class ChatFragment extends Fragment {

    private static final String TAG = "ChatFragment";

    // Layout Views
    private ListView mConversationView;
    private EditText mOutEditText;
    private Button mSendButton;

    private String mContactName;
    private Contact mContact;
    private RealmController database;
    private OpenPgpHandler openPgpHandler;
    ProtocolHandler protocolHandler;


    // Array adapter for the conversation thread
    private ArrayAdapter<String> mConversationArrayAdapter;

    // String buffer for outgoing messages
    private StringBuffer mOutStringBuffer;

    // Name of the connected device
    private String mConnectedDeviceName = null;

    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;

    // Member object for the bluetooth chat services
    private BluetoothChatService mBluetoothChatService = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        database = new RealmController();

        openPgpHandler = new OpenPgpHandler(getContext());
        protocolHandler = new ProtocolHandler(getContext(), mHandler);

        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(getActivity(), "Bluetooth is not available", Toast.LENGTH_LONG).show();
        }

        mContactName = ((ChatActivity) getActivity()).getContactName();

        mContact = database.getContactByName(mContactName);
    }

    @Override
    public void onStart() {
        super.onStart();

        setupChat();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mBluetoothChatService != null) {
            mBluetoothChatService.stop();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mBluetoothChatService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mBluetoothChatService.getState() == BluetoothChatService.STATE_NONE) {
                // Start the Bluetooth chat services
                mBluetoothChatService.start();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chat, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mConversationView = (ListView) view.findViewById(R.id.in);
        mOutEditText = (EditText) view.findViewById(R.id.edit_text_out);
        mSendButton = (Button) view.findViewById(R.id.button_send);
    }

    /**
     * Set up the UI and background operations for chat.
     */
    private void setupChat() {
        Log.d(TAG, "setupChat()");

        // Initialize the array adapter for the conversation thread
        mConversationArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.message);

        mConversationView.setAdapter(mConversationArrayAdapter);

        // Initialize the compose field with a listener for the return key
        mOutEditText.setOnEditorActionListener(mWriteListener);

        // Initialize the send button with a listener that for click events
        mSendButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Send a message using content of the edit text widget
                View view = getView();
                if (null != view) {
                    TextView textView = (TextView) view.findViewById(R.id.edit_text_out);
                    String message = textView.getText().toString();
                    Log.i(TAG, "message send!");
                    sendMessage(message);
                }
            }
        });

        // Initialize the BluetoothChatService to perform bluetooth connections
        mBluetoothChatService = BluetoothChatService.getInstance(getActivity(), mHandler);

        // Initialize the buffer for outgoing messages
        mOutStringBuffer = new StringBuffer("");

//        ProtocolHandler protocolHelper = new ProtocolHandler(getContext(), null);
        List<PersonalMessage> messages = protocolHandler.getMessagesFor(mContactName);

        for (PersonalMessage m : messages) {
            addMessageToAdapter(m);
        }
    }

    private void addMessageToAdapter(PersonalMessage m) {
        String secure = "\uD83D\uDD13";
        if (m.isEncrypted()) {
            secure = "\uD83D\uDD12";
        }

        String messageText = m.getMessage();

        mConversationArrayAdapter.add(m.getFrom() + " | " + secure + " | \u2197" + Utils.localTime(m.getCreation_timestamp()) + " | \u2198" + Utils.localTime(m.getReceived_timestamp()) + ": \n" + messageText);
    }

    /**
     * Makes this device discoverable.
     */
    private void ensureDiscoverable() {
        if (mBluetoothAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }

    /**
     * Sends a message.
     */
    private void sendMessage(String message) {

        // Check that there's actually something to send
        if (message.length() > 0) {
            // store message to database
            protocolHandler.sendMessage(message, mContact, isSecure());

            // Reset out string buffer to zero and clear the edit text field
            mOutStringBuffer.setLength(0);
            mOutEditText.setText(mOutStringBuffer);

            setupChat();
        }
    }

    /**
     * checks if chat is a secure chat
     */
    private boolean isSecure() {
        return openPgpHandler.isSecure() && openPgpHandler.isSecureChannelForUserAvailable(mContact);
    }

    /**
     * The action listener for the EditText widget, to listen for the return key
     */
    private TextView.OnEditorActionListener mWriteListener
            = new TextView.OnEditorActionListener() {
        public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
            // If the action is a key-up event on the return key, send the message
            if (actionId == EditorInfo.IME_NULL && event.getAction() == KeyEvent.ACTION_UP) {
                String message = view.getText().toString();
                sendMessage(message);
            }
            return true;
        }
    };

    /**
     * Updates the status on the action bar.
     */
    private void setStatus(int resId) {
        FragmentActivity activity = getActivity();
        if (null == activity) {
            return;
        }
        final ActionBar actionBar = activity.getActionBar();
        if (null == actionBar) {
            return;
        }
        actionBar.setSubtitle(resId);
    }

    /**
     * Updates the status on the action bar.
     */
    private void setStatus(CharSequence subTitle) {
        FragmentActivity activity = getActivity();
        if (null == activity) {
            return;
        }
        final ActionBar actionBar = activity.getActionBar();
        if (null == actionBar) {
            return;
        }
        actionBar.setSubtitle(subTitle);
    }

    /**
     * The Handler that gets information back from the BluetoothChatService
     */
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            FragmentActivity activity = getActivity();
            switch (msg.what) {
                case Constants.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothChatService.STATE_CONNECTED:
                            setStatus(getString(R.string.title_connected_to, mConnectedDeviceName));
                            mConversationArrayAdapter.clear();
                            break;
                        case BluetoothChatService.STATE_CONNECTING:
                            setStatus(R.string.title_connecting);
                            break;
                        case BluetoothChatService.STATE_LISTEN:
                        case BluetoothChatService.STATE_NONE:
                            setStatus(R.string.title_not_connected);
                            break;
                    }
                    break;
                case Constants.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    mConversationArrayAdapter.add("Me:  " + writeMessage);
                    break;
                case Constants.MESSAGE_READ:
                    Log.i(TAG, "Message delivered to gui");
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    if (readMessage.split(":")[0].equals(mContactName)) {
//                        addMessageToAdapter(System.currentTimeMillis(), mContactName, isSecure(), readMessage);
                        setupChat();
                    } else {
//                        Toast.makeText(getContext(), "New Message: (" + readMessage + ")",
//                                Toast.LENGTH_SHORT).show();
                    }

                    break;
                case Constants.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(Constants.DEVICE_NAME);
                    if (null != activity) {
                        Toast.makeText(activity, "Connected to "
                                + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    }
                    break;
                case Constants.MESSAGE_TOAST:
                    if (null != activity) {
                        Toast.makeText(activity, msg.getData().getString(Constants.TOAST),
                                Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_chat, menu);

        if (isSecure()) {
            menu.findItem(R.id.action_secure).setIcon(android.R.drawable.ic_secure);
        } else {
            menu.findItem(R.id.action_secure).setIcon(android.R.drawable.ic_partial_secure);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_secure: {
                SharedPreferences settings = MainApp.settings;
                boolean encryptionEnabled = settings.getBoolean("encryption_switch", false);

                if (encryptionEnabled) {
                    if (isSecure()) {
                        Toast.makeText(getActivity(), "This chat is encrypted!", Toast.LENGTH_LONG).show();
                        askUserForOpenPGPIdentity();
                    } else {
                        Toast.makeText(getActivity(), "This chat is not encrypted yet! Couldn't find OpenPGP identity " + mContact.getOpenPGPID() + ".", Toast.LENGTH_LONG).show();
                        askUserForOpenPGPIdentity();
                    }
                } else {
                    Intent settingsIntent = new Intent(getContext(), SettingsActivity.class);
                    settingsIntent.putExtra(SettingsActivity.MESSAGE, "Please activate encryption in settings.");
                    settingsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(settingsIntent);
                }
                break;
            }
            case R.id.settings: {
                Log.e(TAG, "open settings");
                Intent myIntent = new Intent(getActivity(), SettingsActivity.class);
                startActivity(myIntent);
                break;
            }
        }
        return false;
    }

    private void askUserForOpenPGPIdentity() {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle("Please enter openPGP identity for this contact.");

            LinearLayout layout = new LinearLayout(getContext());
            layout.setOrientation(LinearLayout.VERTICAL);

            // Set up the input
            final EditText input = new EditText(getContext());

            input.setInputType(InputType.TYPE_CLASS_TEXT);

            String openPGPID = "";
            if (mContact.getOpenPGPID() == null || mContact.getOpenPGPID().isEmpty()) {
                openPGPID = mContact.getName();
            } else {
                openPGPID = mContact.getOpenPGPID();
            }
            input.setText(openPGPID);
            layout.addView(input);
            builder.setView(layout);

            // Set up the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    RealmController realmController = new RealmController();
                    realmController.updateOpenPGPIdentityForContact(mContactName, input.getText().toString());
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

            builder.show();

        } catch (NullPointerException e) {
            Log.e(TAG, "couldn't get users identity");
        }

    }

}
