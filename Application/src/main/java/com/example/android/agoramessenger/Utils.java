package com.example.android.agoramessenger;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.android.agoramessenger.protocol.database.PersonalMessage;
import com.example.android.agoramessenger.protocol.database.TransmissionMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by xx on 30.10.16.
 */
public class Utils {
    private static String hash(String s) {
        try {
            // Create SHA Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("SHA");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String hashMessage(TransmissionMessage message) {
        String toHash = message.getFrom() + message.getTo() + message.getCreation_timestamp() + message.getMessage();
        return hash(toHash);
    }

    public static String hashMessage(PersonalMessage message) {
        String toHash = message.getFrom() + message.getTo() + message.getCreation_timestamp() + message.getMessage();
        return hash(toHash);
    }

    public static JSONObject messageToJson(TransmissionMessage message) {
        JSONObject object = new JSONObject();
        try {
            object.put("from", message.getFrom());
            object.put("to", message.getTo());
            object.put("message", message.getMessage());
            object.put("encrypted", message.getEncrypted());
            object.put("creation_timestamp", message.getCreation_timestamp());
            object.put("hops", message.getHops());
            object.put("hash", message.getHash());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return object;
    }

    public static String messagesToJson(List<TransmissionMessage> messages) {
        JSONObject json_object = new JSONObject();

        try {
            JSONArray json_messages = new JSONArray();

            for (TransmissionMessage message : messages) {
                JSONObject json_message = new JSONObject();
                json_messages.put(Utils.messageToJson(message));
            }

            json_object.put("messages", json_messages);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return json_object.toString();
    }

    public static List<TransmissionMessage> jsonToMessages(String mainObject) {

        List<TransmissionMessage> messages = new ArrayList();

        try {
            JSONObject json_mainObject = new JSONObject(mainObject);

            JSONArray json_messages = json_mainObject.getJSONArray("messages");

            for (int i = 0; i < json_messages.length(); i++) {

                JSONObject json_message = json_messages.getJSONObject(i);

                TransmissionMessage message = new TransmissionMessage();

                message.setFrom(json_message.getString("from"));
                message.setTo(json_message.getString("to"));
                message.setMessage(json_message.getString("message"));
                message.setEncrypted(json_message.getBoolean("encrypted"));
                message.setCreation_timestamp(Long.parseLong(json_message.getString("creation_timestamp")));
                message.setHops(json_message.getInt("hops"));
                message.setHash(json_message.getString("hash"));

                messages.add(message);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return messages;
    }


    /**
     * transform transmission message to personal message and decrypts if necessary
     * returns null if encryption fails
     */
    public static PersonalMessage transmissionToPersonal(TransmissionMessage tMessage) {

        PersonalMessage pMessage = new PersonalMessage(tMessage.getMessage(), tMessage.getFrom(), tMessage.getTo(), tMessage.getCreation_timestamp(), tMessage.getReceived_timestamp(), tMessage.isEncrypted());
        return pMessage;
    }

    public static String localTime(long timestamp) {
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        sdf.setTimeZone(tz);

        String localTime = sdf.format(new Date(timestamp)); // I assume your timestamp is in seconds and you're converting to milliseconds?

        return localTime;
    }

    public static String getUserName() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(MainApp.context);
        String value = settings.getString("username", "");
        return value;
    }
}
