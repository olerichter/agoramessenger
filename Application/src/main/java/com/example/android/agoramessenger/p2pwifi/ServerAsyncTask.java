package com.example.android.agoramessenger.p2pwifi;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by xx on 22.10.16.
 */
public class ServerAsyncTask extends AsyncTask<Void, Void, String> {
  
    private Context context;

    private static final String TAG = "ServerAsyncTask";


    public ServerAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(Void... params) {
        try {

            /**
             * Create a server socket and wait for client connections. This
             * call blocks until a connection is accepted from a client
             */
            ServerSocket serverSocket = new ServerSocket(8888);
            Socket client = serverSocket.accept();



            InputStream inputstream = client.getInputStream();

            serverSocket.close();
            BufferedReader r = new BufferedReader(new InputStreamReader(inputstream));
            StringBuilder total = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                total.append(line).append('\n');
            }

            Log.d(TAG, "Server input: " + total.toString());
            return total.toString();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
            return null;
        }
    }

    /**
     * Start activity that can handle the JPEG image
     */
    @Override
    protected void onPostExecute(String result) {
        if (result != null) {
            Log.v(TAG, "got data: " + result);
        }
    }
}
