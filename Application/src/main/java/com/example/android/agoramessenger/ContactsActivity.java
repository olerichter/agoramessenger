package com.example.android.agoramessenger;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.example.android.agoramessenger.bluetooth.BluetoothChatService;
import com.example.android.agoramessenger.bluetooth.BluetoothDiscoveryReceiver;
import com.example.android.agoramessenger.p2pwifi.ServerAsyncTask;
import com.example.android.agoramessenger.protocol.ProtocolHandler;
import com.example.android.agoramessenger.protocol.database.Contact;

import java.util.ArrayList;
import java.util.List;


public class ContactsActivity extends Activity {

    private static final String TAG = "ContactsActivity";

    private List<String> contactsList;

    private BluetoothChatService mBluetoothChatService;
    /**
     * Local Bluetooth adapter
     */
    private BluetoothAdapter mBluetoothAdapter = null;

    private BluetoothDiscoveryReceiver mReceiver = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        contactsList = new ArrayList<>();

        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            Log.e(Constants.TAG, "Bluetooth is not available");
            this.finish();
        }

        initializeContacts();

        if (!contactsList.isEmpty()) {
            final ListView listview = (ListView) findViewById(R.id.listview);

            final ContactsArrayAdapter adapter = new ContactsArrayAdapter(this,
                    android.R.layout.simple_list_item_1, contactsList);
            listview.setAdapter(adapter);

            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, final View view,
                                        int position, long id) {
                    final String item = (String) parent.getItemAtPosition(position);
                    startChat(item);
                }

            });
        }

        BluetoothChatService mBluetoothChatService = BluetoothChatService.getInstance(this, mHandler);

        initializeBluetoothDiscoveryReceiver();

        // startWifiServer();

    }

    private void startWifiServer() {
        // start wifi server
        new ServerAsyncTask(getApplicationContext());
    }

    private void initializeBluetoothDiscoveryReceiver() {
        // make device discoverable
        makeBluetoothDiscoverable();

        if(mReceiver == null) {
            mReceiver = new BluetoothDiscoveryReceiver();
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);

        this.registerReceiver(mReceiver, filter);
    }

    private void initializeContacts() {
        ProtocolHandler protocolHandler = new ProtocolHandler(this);
        List<Contact> contacts = protocolHandler.getContacts();

        for (Contact contact : contacts) {
            contactsList.add(contact.getName());
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_contacts, menu);
        return true;
    }

    /**
     * Makes this device discoverable.
     */
    private void makeBluetoothDiscoverable() {
        if (mBluetoothAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.makeBluetoothDiscoverable: {
                makeBluetoothDiscoverable();
                return true;
            }
            case R.id.new_chat: {
                newMessage();
                return true;
            }
            case R.id.start_bluetooth_service: {
                startBluetoothService();
                break;
            }
            case R.id.stop_bluetooth_service: {
                stopBluetoothService();
                break;
            }
            case R.id.start_wifi_service: {
                startWifiService();
                break;
            }
            case R.id.stop_wifi_service: {
                stopWifiService();
                break;
            }
            case R.id.settings: {
                Intent intent = new Intent(ContactsActivity.this, SettingsActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.log_action: {
                Intent intent = new Intent(ContactsActivity.this, LogTabActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.test_action: {
//                test();
                break;
            }
        }
        return false;
    }

    private void startBluetoothService() {
        makeBluetoothDiscoverable();

        BluetoothChatService bluetoothChatService = BluetoothChatService.getInstance(this, null);
        bluetoothChatService.start();
        bluetoothChatService.startService();
    }

    private void stopBluetoothService() {
        BluetoothChatService bluetoothChatService = BluetoothChatService.getInstance(this, null);
        bluetoothChatService.stopService();
    }

    private void startWifiService() {

    }

    private void stopWifiService() {

    }

    @Override
    protected void onDestroy() {
        // unregister the receiver
        if (mReceiver != null) {
            this.unregisterReceiver(mReceiver);
            mReceiver = null;
        }

        super.onDestroy();
    }

    private void startChat(String name) {
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(ChatActivity.CONTACT_NAME_MESSAGE, name);
        startActivity(intent);
    }


    private void newMessage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Please enter user name!");

        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);

        // Set up the input
        final EditText name = new EditText(this);
        name.setInputType(InputType.TYPE_CLASS_TEXT);
        name.setHint("Name");
        layout.addView(name);

        builder.setView(layout);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ProtocolHandler protocolHelper = new ProtocolHandler(getApplicationContext(), null);
                String contactName = name.getText().toString();
                if (!contactName.equals("")) {
                    protocolHelper.createContact(contactName);
                    startChat(name.getText().toString());
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        builder.show();
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Constants.MESSAGE_READ:

                    com.example.android.common.logger.Log.i(TAG, "Message delivered to gui");
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);

                    Toast.makeText(getApplication(), "New Message: (" + readMessage + ")",
                            Toast.LENGTH_SHORT).show();

                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                    break;
            }
        }
    };
}
