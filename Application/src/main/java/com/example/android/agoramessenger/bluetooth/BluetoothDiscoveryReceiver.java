package com.example.android.agoramessenger.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.android.agoramessenger.bluetooth.BluetoothChatService;
import com.example.android.agoramessenger.protocol.ProtocolHandler;

import android.util.Log;

import java.util.Random;

/**
 * Created by xx on 21.12.15.
 */
public class BluetoothDiscoveryReceiver extends BroadcastReceiver {
    private BluetoothAdapter mBtAdapter;
    private static final String TAG = "BluetoothDiscoveryRcv";

    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();

        Log.d(TAG, "received something! " + action);


        // When discovery finds a device
        if (BluetoothDevice.ACTION_FOUND.equals(action)) {
            // Get the BluetoothDevice object from the Intent
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            Log.i(TAG, "Name: " + device.getName() + ", Address:" + device.getAddress());


            // ask database if we should connect to device
            ProtocolHandler protocolHelper = new ProtocolHandler(context, null);
            if (protocolHelper.isReadyToConnect(device.getAddress())) {
                BluetoothDevice btDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(device.getAddress());

                // Attempt to connect to the device
                BluetoothChatService chatService = BluetoothChatService.getInstance(context, null);

                // delay connections for x seconds
                Random r = new Random();
                int delay = r.nextInt(500);
                try {
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                chatService.connect(btDevice, false);
            }

            // When discovery is finished
        } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
            Log.d(TAG, "discovery finished!");

        } else {
            Log.d(TAG, action);
        }
    }
}
