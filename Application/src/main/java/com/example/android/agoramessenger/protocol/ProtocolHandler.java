package com.example.android.agoramessenger.protocol;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.android.agoramessenger.Constants;
import com.example.android.agoramessenger.Utils;
import com.example.android.agoramessenger.protocol.crypto.OpenPgpHandler;
import com.example.android.agoramessenger.protocol.database.ConnectedDevice;
import com.example.android.agoramessenger.protocol.database.Contact;
import com.example.android.agoramessenger.protocol.database.PersonalMessage;
import com.example.android.agoramessenger.protocol.database.RealmController;
import com.example.android.agoramessenger.protocol.database.TransmissionMessage;

import java.util.List;

/**
 * Created by xx on 21.12.15.
 */
public class ProtocolHandler {

    RealmController realmController;

    // ToDo into constants

    // return values for processCommand
    public static final int RETURN_VALUE_NORMAL = 1;
    public static final int RETURN_VALUE_NOMORETOSEND = 0;
    public static final int RETURN_VALUE_ERROR = 2;

    public static final String CMD_MESSAGE = "msg";
    public static final String CMD_NOMORETOSEND = "noMoreToSend";

    private static final String TAG = "ProtocolHelper";

    public static String name;

    private Context mContext;
    private Handler mHandler;

    OpenPgpHandler openPgpApiHelper;


    public ProtocolHandler(Context context, Handler handler) {
        this.mContext = context;
        this.mHandler = handler;
        this.realmController = new RealmController();
        this.openPgpApiHelper = new OpenPgpHandler(mContext);
    }

    public ProtocolHandler(Context context) {
        this.mContext = context;
        this.mHandler = null;
        this.realmController = new RealmController();
        this.openPgpApiHelper = new OpenPgpHandler(mContext);
    }

    public String getUserName() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);
        String value = settings.getString("username", "");
        return value;
    }

    /**
     * get Messages (in transport format with command) from realmController to send to the device with the given address
     */
    public List<TransmissionMessage> getMessagesForDevice(String address) {
        ConnectedDevice device = realmController.getDeviceFor(address);

        if (device == null) {
            return realmController.getTransmissionMessages();
        }

        return realmController.getTransmissionMessagesFor(device);
    }

    /**
     * stores the timestamp for the connection in the db and starts cleaning process
     */
    public void connectToDevice(String address) {
        ConnectedDevice device = new ConnectedDevice();
        device.setAddress(address);
        device.setTimestamp(System.currentTimeMillis());

        realmController.updateDevice(device);

        startCleaningTask();
    }

    /**
     * store message (just text) from user input and encrypt if secure is true
     */
    public PersonalMessage sendMessage(String message, Contact to, boolean secure) {
        String from = getUserName();
        long timestamp = System.currentTimeMillis();

        //store personal message
        PersonalMessage personalMessage = new PersonalMessage(message, from, to.getName(), timestamp, timestamp, secure);
        storePersonalMessage(personalMessage);

        // send personal message to gui
        sendToGui(personalMessage);

        // store transmission message
        int hops = 0;
        TransmissionMessage transmissionMessage;

        if (secure) {
            String encryptedMessage = encryptMessage(message, to);
            Log.i(TAG, "Encrypted Message: " + encryptedMessage);
            transmissionMessage = new TransmissionMessage(encryptedMessage, from, to.getName(), timestamp, timestamp, hops, true);
        } else {
            transmissionMessage = new TransmissionMessage(message, from, to.getName(), timestamp, timestamp, hops, false);
        }

        storeTransmissionMessage(transmissionMessage);

        return personalMessage;
    }

    /**
     * encrypts message
     */
    private String encryptMessage(String message, Contact to) {
        String encryptedMessage;
        encryptedMessage = openPgpApiHelper.signAndEncrypt(new Intent(), message, to);
        return encryptedMessage;
    }

    /**
     * should we connect to the device with the given address?
     */
    public boolean isReadyToConnect(String address) {
        ConnectedDevice device = realmController.getDeviceFor(address);
        long lastMessage = realmController.getNewestMessage();

        if (device == null || lastMessage > device.getTimestamp()) {
            Log.d(TAG, "Connect to device: " + address);
            return true;
        } else {
            Log.d(TAG, "Don't connect to device: " + address);
            return false;
        }
    }

    /**
     * decide what to do with message (in transport format) from the given address
     */
    public void processMessages(List<TransmissionMessage> messages) {
        for (TransmissionMessage transmissionMessage : messages) {
            transmissionMessage.setReceived_timestamp(System.currentTimeMillis());

            // if message is for user store and decrypt
            if (transmissionMessage.getTo().equals(getUserName())) {
                PersonalMessage personalMessage = Utils.transmissionToPersonal(transmissionMessage);

                // decrypt if necessary
                if (personalMessage.isEncrypted() && personalMessage.getTo().equals(Utils.getUserName())) {
                    personalMessage.setMessage(decryptMessage(personalMessage.getMessage()));
                }

                if (personalMessage != null) {
                    sendToGui(personalMessage);
                    storePersonalMessage(personalMessage);
                }
            } else {
                storeTransmissionMessage(transmissionMessage);
            }
        }


    }

    /**
     * send message to gui
     */
    private void sendToGui(PersonalMessage message) {
        if (mHandler != null) {
            String str = message.getFrom() + ": " + message.getMessage();
            mHandler.obtainMessage(Constants.MESSAGE_READ, str.length(), -1, str.getBytes()).sendToTarget();
        }
    }

    /**
     * store transmission message in realmController and create new contact if necessary
     */
    private void storeTransmissionMessage(TransmissionMessage message) {
        realmController.addTransmissionMessage(message);
    }

    /**
     * store personal message in realmController
     */
    private void storePersonalMessage(PersonalMessage message) {
        realmController.addPersonalMessage(message);

        // create or touch contact
        if (!message.getFrom().equals(getUserName())) {
            Contact newContact = new Contact(message.getFrom(), System.currentTimeMillis());
            realmController.addContact(newContact);
        } else if (!message.getTo().equals(getUserName())) {
            Contact newContact = new Contact(message.getTo(), System.currentTimeMillis());
            realmController.addContact(newContact);
        }
    }

    public List<PersonalMessage> getMessagesFor(String name) {
        Contact contact = realmController.getContactByName(name);
        return realmController.getPersonalMessagesFor(contact);
    }

    public List<Contact> getContacts() {
        return realmController.getContacts();
    }

    public String decryptMessage(String message) {
        String decryptedMessage = "";
//        OpenPgpHandler openPgpApiHelper = new OpenPgpHandler(mContext);

        Log.d(Constants.TAG, "encrypted message: " + message);


        decryptedMessage = openPgpApiHelper.decryptAndVerify(new Intent(), message);
//        openPgpApiHelper.decryptAndVerifyAsync(new Intent(), message);
        Log.d(Constants.TAG, "decrypted message: " + decryptedMessage);

        return decryptedMessage;
    }

    public void startCleaningTask() {

        realmController.cleanOldMessages();
    }

    public void createContact(String name) {
        Contact contact = new Contact(name);
        realmController.addContact(contact);
    }

}
