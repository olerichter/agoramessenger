package com.example.android.agoramessenger.protocol.database;

import com.example.android.agoramessenger.Utils;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by xx on 21.12.15.
 */
public class TransmissionMessage extends RealmObject {
    @PrimaryKey
    private String hash;

    private String message;
    private String from;
    private String to;
    private long creation_timestamp;
    private long received_timestamp;
    private long hops;
    private boolean encrypted;


    public TransmissionMessage() {

    }


    public TransmissionMessage(String message, String from, String to, long creation_timestamp, long received_timestamp, long hops, boolean encrypted) {
        this.message = message;
        this.from = from;
        this.to = to;
        this.creation_timestamp = creation_timestamp;
        this.received_timestamp = received_timestamp;
        this.hops = hops;
        this.encrypted = encrypted;
        this.hash = Utils.hashMessage(this);
    }

    public boolean isEncrypted() {
        return getEncrypted();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public long getCreation_timestamp() {
        return creation_timestamp;
    }

    public void setCreation_timestamp(long creation_timestamp) {
        this.creation_timestamp = creation_timestamp;
    }

    public long getReceived_timestamp() {
        return received_timestamp;
    }

    public void setReceived_timestamp(long received_timestamp) {
        this.received_timestamp = received_timestamp;
    }

    public long getHops() {
        return hops;
    }

    public void setHops(long hops) {
        this.hops = hops;
    }

    public void setEncrypted(boolean encrypted) {
        this.encrypted = encrypted;
    }

    public boolean getEncrypted() {
        return encrypted;
    }


    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}
