package com.example.android.agoramessenger.protocol.database;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by xx on 30.10.16.
 */
public class Contact extends RealmObject{
    @PrimaryKey
    private String name;
    private long timestamp_lastMessage;
    private String openPGPID;

    public Contact() {

    }

    public Contact(String name, long timestamp_lastMessage) {
        this.name = name;
        this.timestamp_lastMessage = timestamp_lastMessage;
    }

    public Contact(String name) {
        this.name = name;
        this.timestamp_lastMessage = 0;
        this.openPGPID = null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTimestamp_lastMessage() {
        return timestamp_lastMessage;
    }

    public void setTimestamp_lastMessage(long timestamp_lastMessage) {
        this.timestamp_lastMessage = timestamp_lastMessage;
    }

    public void setOpenPGPID(String openPGPID) {
        this.openPGPID = openPGPID;
    }

    public String getOpenPGPID() {
        return this.openPGPID;
    }
}
