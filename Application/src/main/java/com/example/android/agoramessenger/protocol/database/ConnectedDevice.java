package com.example.android.agoramessenger.protocol.database;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by xx on 22.12.15.
 */
public class ConnectedDevice extends RealmObject {
    @PrimaryKey
    private String address;
    private long timestamp;

    public ConnectedDevice() {
    }

    public ConnectedDevice(String address, long timestamp) {
        this.address = address;
        this.timestamp = timestamp;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
