package com.example.android.agoramessenger.p2pwifi;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;

/**
 * Created by xx on 22.10.16.
 */
public class ClientAsyncTask extends AsyncTask<Void, Void, Void> {

    private Context context;
    private String host;
    private int port;

    private static final String TAG = "ClientAsyncTask";


    public ClientAsyncTask(Context context, String host, int port) {
        this.context = context;
        this.host = host;
        this.port = port;
    }

    @Override
    protected Void doInBackground(Void... params) {

        Socket socket = new Socket();

        try {
            /**
             * Create a client socket with the host,
             * port, and timeout information.
             */
            socket.bind(null);
            socket.connect((new InetSocketAddress(host, port)), 8888);


            OutputStream outputStream = socket.getOutputStream();
            outputStream.write("hallo".getBytes(Charset.forName("UTF-8")));
            outputStream.close();
            Log.d(TAG, "Client output: " + "hallo");

        } catch (FileNotFoundException e) {
            //catch logic
        } catch (IOException e) {
            //catch logic
        }

        finally {
            if (socket != null) {
                if (socket.isConnected()) {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        //catch logic
                    }
                }
            }
        }

        return null;
    }


    /**
     * Start activity that can handle the JPEG image
     */
    @Override
    protected void onPostExecute(Void result) {

    }
}
