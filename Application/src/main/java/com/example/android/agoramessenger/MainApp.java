package com.example.android.agoramessenger;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.android.agoramessenger.p2pwifi.ConnectionService;
import com.example.android.agoramessenger.protocol.ProtocolHandler;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by xx on 30.10.16.
 */
public class MainApp extends Application {

    // to access context from everywhere
    public static Context context;
    public static SharedPreferences settings;
    private ProtocolHandler protocolHandler;

    private static final String TAG = "MainApp";

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate() {
        super.onCreate();


        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(9)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);

        context = getApplicationContext();
        settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        protocolHandler = new ProtocolHandler(this);

//        startWifiService();

        // check for username
        if (protocolHandler.getUserName().equals("")) {
            Intent intent = new Intent(this, SettingsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(SettingsActivity.MESSAGE, "Please set your username!");
            startActivity(intent);
        }


    }

    private void startWifiService() {
        // If service not started yet, start it.
        Intent serviceIntent = new Intent(this, ConnectionService.class);
        startService(serviceIntent);  // start the connection service
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

}