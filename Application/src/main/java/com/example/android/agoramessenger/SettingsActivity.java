package com.example.android.agoramessenger;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.SwitchPreference;
import android.view.MenuItem;
import android.widget.Toast;

import org.openintents.openpgp.util.OpenPgpAppPreference;
import org.openintents.openpgp.util.OpenPgpKeyPreference;

public class SettingsActivity extends PreferenceActivity {
    public static final String MESSAGE = "agoramessenger.SETTINGS_MESSAGE";
    EditTextPreference mUsername;
    OpenPgpKeyPreference mKey;
    OpenPgpAppPreference mProvider;
    EditTextPreference mPassphrase;
    SwitchPreference mEncryption;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setIcon(null);


        // load preferences from xml
        addPreferencesFromResource(R.xml.base_preference);

        mUsername = (EditTextPreference) findPreference("username");

        // find preferences
        mProvider = (OpenPgpAppPreference) findPreference("openpgp_provider");
        mKey = (OpenPgpKeyPreference) findPreference("openpgp_key");
        mPassphrase = (EditTextPreference) findPreference("openpgp_passphrase");
        mEncryption = (SwitchPreference) findPreference("encryption_switch");

        mProvider.setDefaultValue("org.sufficientlysecure:openpgp-api:11.0");

        mKey.setOpenPgpProvider(mProvider.getValue());
        mProvider.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                mKey.setOpenPgpProvider((String) newValue);
                return true;
            }
        });

        if (getIntent().hasExtra(MESSAGE)) {
            Toast.makeText(this, getIntent().getStringExtra(MESSAGE),
                    Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mKey.handleOnActivityResult(requestCode, resultCode, data)) {
            // handled by OpenPgpKeyPreference
            return;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
