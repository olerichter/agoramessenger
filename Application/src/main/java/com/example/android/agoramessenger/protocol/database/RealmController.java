package com.example.android.agoramessenger.protocol.database;

import android.bluetooth.BluetoothClass;
import android.util.Log;

import com.example.android.agoramessenger.Constants;

import java.io.IOException;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.exceptions.RealmException;


/**
 * Created by xx on 21.12.15.
 */
public class RealmController {
    private static final String TAG = "RealmController";
    private final Realm realm;


    public RealmController() {
        realm = Realm.getDefaultInstance();
    }

    public void addContact(Contact contact) {
        if (isContact(contact)) {
            Contact storedContact = realm.where(Contact.class)
                    .equalTo("name", contact.getName())
                    .findFirst();

            if (storedContact != null) {
                realm.beginTransaction();
                storedContact.setTimestamp_lastMessage(contact.getTimestamp_lastMessage());
                realm.commitTransaction();
            }
        } else {
            realm.beginTransaction();
            realm.copyToRealm(contact);
            realm.commitTransaction();
            Log.v(TAG, "Contact added: " + contact.getName());
        }
    }

    public boolean isContact(Contact contact) {
        RealmResults<Contact> contacts = realm.where(Contact.class)
                .equalTo("name", contact.getName())
                .findAll();

        if (contacts.isEmpty())
            return false;
        return true;
    }

    public void addTransmissionMessage(TransmissionMessage message) {
        if (!isTransmissionMessage(message)) {
            realm.beginTransaction();
            realm.copyToRealm(message);
            realm.commitTransaction();
            Log.v(TAG, "Transmission message added: " + message.getMessage());
        }
    }

    public boolean isTransmissionMessage(TransmissionMessage message) {
        long results = realm.where(TransmissionMessage.class)
                .equalTo("hash", message.getHash())
                .count();

        if (results > 0)
            return true;
        return false;
    }

    public void addPersonalMessage(PersonalMessage message) {
        if (!isPersonalMessage(message)) {
            realm.beginTransaction();
            realm.copyToRealm(message);
            realm.commitTransaction();
            Log.v(TAG, "PersonalMessage added: " + message.getMessage());
        }
    }

    public boolean isPersonalMessage(PersonalMessage message) {
        long results = realm.where(PersonalMessage.class)
                .equalTo("hash", message.getHash())
                .count();

        if (results > 0)
            return true;
        return false;
    }

    public void addConnectedDevice(ConnectedDevice device) {
        try {
            if (!isConnectedDevice(device)) {
                realm.beginTransaction();
                realm.copyToRealm(device);
                realm.commitTransaction();
                Log.v(TAG, "ConnectedDevice added: " + device.getAddress());
            }
        } catch (RealmException e) {
            Log.e(TAG, "add new device failed", e);
        }


    }

    public boolean isConnectedDevice(ConnectedDevice device) {
        long results = realm.where(ConnectedDevice.class)
                .equalTo("address", device.getAddress())
                .count();

        if (results > 0)
            return true;
        return false;
    }

    public List<PersonalMessage> getPersonalMessages() {
        return realm.where(PersonalMessage.class)
                .findAllSorted("received_timestamp");
    }

    public List<PersonalMessage> getPersonalMessagesFor(Contact contact) {
        return realm.where(PersonalMessage.class)
                .equalTo("from", contact.getName())
                .or()
                .equalTo("to", contact.getName())
                .findAllSorted("creation_timestamp");
    }

    public List<TransmissionMessage> getTransmissionMessages() {
        return realm.where(TransmissionMessage.class)
                .findAllSorted("received_timestamp");
    }

    public List<TransmissionMessage> getTransmissionMessagesFor(ConnectedDevice device) {
        return realm.where(TransmissionMessage.class)
                .greaterThan("received_timestamp", device.getTimestamp())
                .findAllSorted("received_timestamp");
    }


    public long getNewestMessage() {
        return realm.where(TransmissionMessage.class)
                .maximumInt("received_timestamp");
    }

    public ConnectedDevice getDeviceFor(String address) {
        return realm.where(ConnectedDevice.class)
                .equalTo("address", address)
                .findFirst();
    }

    public void updateDevice(ConnectedDevice device) {
        ConnectedDevice storedDevice = realm.where(ConnectedDevice.class)
                .equalTo("address", device.getAddress())
                .findFirst();

        if (storedDevice != null) {
            realm.beginTransaction();
            storedDevice.setTimestamp(device.getTimestamp());
            realm.commitTransaction();
        } else {
            addConnectedDevice(device);
        }
    }

    public List<Contact> getContacts() {
        return realm.where(Contact.class).findAllSorted("timestamp_lastMessage", false);
    }

    public List<ConnectedDevice> getDevices() {
        return realm.where(ConnectedDevice.class).findAllSorted("timestamp", false);
    }

    public Contact getContactByName(String name) {
        return realm.where(Contact.class)
                .equalTo("name", name)
                .findFirst();
    }

    public void updateOpenPGPIdentityForContact(String name, String openPGPIdentity) {
        Contact storedContact = realm.where(Contact.class)
                .equalTo("name", name)
                .findFirst();

        if (storedContact != null) {
            realm.beginTransaction();
            Contact contact = getContactByName(name);
            contact.setOpenPGPID(openPGPIdentity);
            realm.commitTransaction();
        }
    }

    public void cleanOldMessages() {
        long messageCount = realm.where(TransmissionMessage.class).count();

        long timestampOldestMessage = System.currentTimeMillis() - Constants.MAX_MESSAGE_AGE * 1000;
        long max_messages = Constants.MAX_MESSAGES;

        if (messageCount > max_messages) {
            RealmResults<TransmissionMessage> rows = realm.where(TransmissionMessage.class).lessThan("creation_timestamp", timestampOldestMessage).findAll();
            rows.clear();
        }
    }
}
