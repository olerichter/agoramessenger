package com.example.android.agoramessenger;

import android.app.Activity;
import android.app.ActionBar;
import android.app.FragmentTransaction;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.example.android.agoramessenger.protocol.database.ConnectedDevice;
import com.example.android.agoramessenger.protocol.database.Contact;
import com.example.android.agoramessenger.protocol.database.PersonalMessage;
import com.example.android.agoramessenger.protocol.database.RealmController;
import com.example.android.agoramessenger.protocol.database.TransmissionMessage;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class LogTabActivity extends Activity implements ActionBar.TabListener {
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_tab);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setIcon(null);

        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_log_tab, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in
        // the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return TransmissionMessageLogFragment.newInstance();
                case 1:
                    return PersonalMessageLogFragment.newInstance();
                case 2:
                    return ContactsLogFragment.newInstance();
                case 3:
                    return DevicesLogFragment.newInstance();
                default:
                    return TransmissionMessageLogFragment.newInstance();
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "TransmissionMessages";
                case 1:
                    return "PersonalMessages";
                case 2:
                    return "Contacts";
                case 3:
                    return "Devices";
            }
            return null;
        }
    }

    public static class TransmissionMessageLogFragment extends Fragment {
        public TransmissionMessageLogFragment() {
        }

        public static TransmissionMessageLogFragment newInstance() {
            TransmissionMessageLogFragment fragment = new TransmissionMessageLogFragment();
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            RealmController realmController = new RealmController();
            List<TransmissionMessage> messages = realmController.getTransmissionMessages();

            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            String text = "";
            for (TransmissionMessage m : messages) {
                text += "From: " + m.getFrom() + "\n";
                text += "To: " + m.getTo() + "\n";
                text += "Message: " + m.getMessage() + "\n";
                text += "Encrypted?: " + m.getEncrypted() + "\n";
                text += "Created: " + formatter.format(m.getCreation_timestamp()) + "\n";
                text += "Received: " + formatter.format(m.getReceived_timestamp()) + "\n";
                text += "Encrypted: " + m.isEncrypted() + "\n \n";


            }

            View rootView = inflater.inflate(R.layout.fragment_log_tab, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setMovementMethod(new ScrollingMovementMethod());
            textView.setText(text);

            return rootView;
        }
    }

    public static class PersonalMessageLogFragment extends Fragment {
        public PersonalMessageLogFragment() {
        }

        public static PersonalMessageLogFragment newInstance() {
            PersonalMessageLogFragment fragment = new PersonalMessageLogFragment();
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            RealmController realmController = new RealmController();

            List<PersonalMessage> messages = realmController.getPersonalMessages();
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            String text = "";
            for (PersonalMessage m : messages) {
                text += "From: " + m.getFrom() + "\n";
                text += "To: " + m.getTo() + "\n";
                text += "Message: " + m.getMessage() + "\n";
                text += "Created: " + formatter.format(m.getCreation_timestamp()) + "\n";
                text += "Received: " + formatter.format(m.getReceived_timestamp()) + "\n";
                text += "Encrypted: " + m.getEncrypted() + "\n \n";
            }

            View rootView = inflater.inflate(R.layout.fragment_log_tab, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setMovementMethod(new ScrollingMovementMethod());
            textView.setText(text);
            return rootView;
        }
    }

    public static class ContactsLogFragment extends Fragment {
        public ContactsLogFragment() {
        }

        public static ContactsLogFragment newInstance() {
            ContactsLogFragment fragment = new ContactsLogFragment();
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            RealmController realmController = new RealmController();
            List<Contact> contacts = realmController.getContacts();

            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            String text = "";
            for (Contact c : contacts) {
                text += "Name: " + c.getName() + "\n";
                text += "PGPID: " + c.getOpenPGPID() + "\n";
                ;
                text += "Last Message: " + formatter.format(c.getTimestamp_lastMessage()) + "\n \n";
            }

            View rootView = inflater.inflate(R.layout.fragment_log_tab, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setMovementMethod(new ScrollingMovementMethod());
            textView.setText(text);
            return rootView;
        }
    }

    public static class DevicesLogFragment extends Fragment {
        public DevicesLogFragment() {
        }

        public static DevicesLogFragment newInstance() {
            DevicesLogFragment fragment = new DevicesLogFragment();
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            RealmController realmController = new RealmController();
            List<ConnectedDevice> devices = realmController.getDevices();

            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            String text = "";
            for (ConnectedDevice d : devices) {
                text += "Address: " + d.getAddress() + "\n";
                text += "Timestamp: " + formatter.format(d.getTimestamp()) + "\n \n";
            }

            View rootView = inflater.inflate(R.layout.fragment_log_tab, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setMovementMethod(new ScrollingMovementMethod());
            textView.setText(text);
            return rootView;
        }
    }
}
