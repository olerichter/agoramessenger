package com.example.android.agoramessenger.protocol.crypto;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.example.android.agoramessenger.Constants;
import com.example.android.agoramessenger.MainApp;
import com.example.android.agoramessenger.SettingsActivity;
import com.example.android.agoramessenger.protocol.database.Contact;

import org.openintents.openpgp.IOpenPgpService2;
import org.openintents.openpgp.OpenPgpDecryptionResult;
import org.openintents.openpgp.OpenPgpError;
import org.openintents.openpgp.OpenPgpSignatureResult;
import org.openintents.openpgp.util.OpenPgpApi;
import org.openintents.openpgp.util.OpenPgpServiceConnection;
import org.openintents.openpgp.util.OpenPgpUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

public class OpenPgpHandler implements OpenPgpServiceConnection.OnBound {


    private static final String TAG = "OpenPgpHandler";
    private OpenPgpServiceConnection mServiceConnection;

    public static final int REQUEST_CODE_CLEARTEXT_SIGN = 9910;
    public static final int REQUEST_CODE_ENCRYPT = 9911;
    public static final int REQUEST_CODE_SIGN_AND_ENCRYPT = 9912;
    public static final int REQUEST_CODE_DECRYPT_AND_VERIFY = 9913;
    public static final int REQUEST_CODE_GET_KEY = 9914;
    public static final int REQUEST_CODE_GET_KEY_IDS = 9915;
    public static final int REQUEST_CODE_DETACHED_SIGN = 9916;
    public static final int REQUEST_CODE_DECRYPT_AND_VERIFY_DETACHED = 9917;
    public static final int REQUEST_CODE_BACKUP = 9918;

    private Context mContext;
    private long mSignKeyId;
    private String mSignKeyPassphrase;
    String providerPackageName;

    public OpenPgpHandler(Context context) {
        this.mContext = context;

        SharedPreferences settings = MainApp.settings;

        providerPackageName = settings.getString("openpgp_provider", "");
        mSignKeyId = settings.getLong("openpgp_key", 0);

        //ToDo passphrase ordentlich sichern
        mSignKeyPassphrase = getPassphrase();

        if (TextUtils.isEmpty(providerPackageName)) {
            Log.e("OpenPGPApiHelper", "no provider name");
            errorHandling("Please select an OpenPGP provider or deactivate encryption!");
        } else if (mSignKeyId == 0) {
            Log.e("OpenPGPApiHelper", "no sign key");
            errorHandling("Please select a key or deactivate encryption!");
        } else if (TextUtils.isEmpty(mSignKeyPassphrase)) {
            Log.e("OpenPGPApiHelper", "no sign key passphrase");
            errorHandling("Please enter your passphrase or deactivate encryption!");
        } else {
            // bind to service
            mServiceConnection = new OpenPgpServiceConnection(
                    mContext.getApplicationContext(),
                    providerPackageName,
                    new OpenPgpServiceConnection.OnBound() {
                        @Override
                        public void onBound(IOpenPgpService2 service) {
                            Log.d(OpenPgpApi.TAG, "onBound!");
                        }

                        @Override
                        public void onError(Exception e) {
                            Log.e(OpenPgpApi.TAG, "exception when binding!", e);
                        }
                    }
            );
            mServiceConnection.bindToService();

            Log.v(TAG, "started PGP with provider: " + providerPackageName);
        }
    }

    public boolean isSecure() {
        SharedPreferences settings = MainApp.settings;
        return settings.getBoolean("encryption_switch", false);
    }

    private void errorHandling(String message) {
        if (isSecure()) {
            Intent intent = new Intent(mContext, SettingsActivity.class);
            intent.putExtra(SettingsActivity.MESSAGE, message);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
        }
    }

    private String getPassphrase() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);
        return settings.getString("openpgp_passphrase", "");
    }

    /**
     *
     */
    public boolean isPassphraseAvailable() {
        if (TextUtils.isEmpty(mSignKeyPassphrase)) {
            return false;
        }
        return true;
    }

    private void showToast(final String message) {
        Toast.makeText(mContext,
                message,
                Toast.LENGTH_SHORT).show();
    }

    /**
     * Takes input from message or ciphertext EditText and turns it into a ByteArrayInputStream
     */
    private InputStream getInputstream(String inputStr) {
        InputStream is = null;
        try {
            is = new ByteArrayInputStream(inputStr.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            Log.e("OpenPgpApi", "UnsupportedEncodingException", e);
        }

        return is;
    }

    private class MyCallback implements OpenPgpApi.IOpenPgpCallback {
        boolean returnToCiphertextField;
        ByteArrayOutputStream os;
        int requestCode;

        private MyCallback(boolean returnToCiphertextField, ByteArrayOutputStream os, int requestCode) {
            this.returnToCiphertextField = returnToCiphertextField;
            this.os = os;
            this.requestCode = requestCode;
        }

        @Override
        public void onReturn(Intent result) {
            switch (result.getIntExtra(OpenPgpApi.RESULT_CODE, OpenPgpApi.RESULT_CODE_ERROR)) {
                case OpenPgpApi.RESULT_CODE_SUCCESS: {
                    showToast("RESULT_CODE_SUCCESS");

                    switch (requestCode) {
                        case REQUEST_CODE_SIGN_AND_ENCRYPT: {
                            try {
                                String passContent = os.toString("UTF-8");
                                showToast("encrypted and signed: " + passContent);
                                Log.i(Constants.TAG, "encrypted: " + passContent);


                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            break;
                        }
                        case REQUEST_CODE_ENCRYPT: {
                            try {
                                String passContent = os.toString("UTF-8");
                                showToast("encrypted: " + passContent);
                                Log.i(Constants.TAG, "encrypted: " + passContent);
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            break;
                        }
                        case REQUEST_CODE_DECRYPT_AND_VERIFY: {
                            OpenPgpSignatureResult signatureResult
                                    = result.getParcelableExtra(OpenPgpApi.RESULT_SIGNATURE);
                            showToast(signatureResult.toString());
                            OpenPgpDecryptionResult decryptionResult
                                    = result.getParcelableExtra(OpenPgpApi.RESULT_DECRYPTION);
                            showToast(decryptionResult.toString());

                            try {
                                String passContent = os.toString("UTF-8");
                                showToast("decrypted: " + passContent);
                                Log.i(Constants.TAG, "decrypted: " + passContent);
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            if (os != null && returnToCiphertextField) {

                            }
                            break;
                        }
                        case REQUEST_CODE_DECRYPT_AND_VERIFY_DETACHED: {
                            // RESULT_SIGNATURE and RESULT_DECRYPTION are never null!

                            OpenPgpSignatureResult signatureResult
                                    = result.getParcelableExtra(OpenPgpApi.RESULT_SIGNATURE);
                            showToast(signatureResult.toString());
                            OpenPgpDecryptionResult decryptionResult
                                    = result.getParcelableExtra(OpenPgpApi.RESULT_DECRYPTION);
                            showToast(decryptionResult.toString());
                            try {
                                String passContent = os.toString("UTF-8");
                                showToast("decrypted: " + passContent);
                                Log.i(Constants.TAG, "decrypted: " + passContent);
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            break;
                        }
                        case REQUEST_CODE_DETACHED_SIGN: {
                            byte[] detachedSig
                                    = result.getByteArrayExtra(OpenPgpApi.RESULT_DETACHED_SIGNATURE);
                            Log.d(OpenPgpApi.TAG, "RESULT_DETACHED_SIGNATURE: " + detachedSig.length
                                    + " str=" + new String(detachedSig));

                            break;
                        }
                        case REQUEST_CODE_GET_KEY_IDS: {
                            long[] keyIds = result.getLongArrayExtra(OpenPgpApi.RESULT_KEY_IDS);
                            String out = "keyIds: ";
                            for (long keyId : keyIds) {
                                out += OpenPgpUtils.convertKeyIdToHex(keyId) + ", ";
                            }
                            showToast(out);
                            Log.i(Constants.TAG, out);
                            break;
                        }
                        default: {

                        }
                    }

                    break;
                }
                case OpenPgpApi.RESULT_CODE_USER_INTERACTION_REQUIRED: {
                    showToast("RESULT_CODE_USER_INTERACTION_REQUIRED");
                }
                case OpenPgpApi.RESULT_CODE_ERROR: {
                    showToast("RESULT_CODE_ERROR");

                    OpenPgpError error = result.getParcelableExtra(OpenPgpApi.RESULT_ERROR);
                    showToast(error.getMessage());

//                    handleError(error);
                    break;
                }
            }
        }
    }

    public String encrypt(Intent data, String input, String[] userIds) {
        data.setAction(OpenPgpApi.ACTION_ENCRYPT);
        data.putExtra(OpenPgpApi.EXTRA_USER_IDS, userIds);
        data.putExtra(OpenPgpApi.EXTRA_REQUEST_ASCII_ARMOR, true);

        InputStream is = getInputstream(input);
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        OpenPgpApi api = new OpenPgpApi(mContext, mServiceConnection.getService());
        api.executeApi(data, is, os);

        String output = "";
        try {
            output = os.toString("UTF-8");
            Log.i(Constants.TAG, "encrypted: " + output);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (output.isEmpty()) {
            Log.e(Constants.TAG, "message couldn't be encrypted");
        } else {
            Log.i(Constants.TAG, "message encrypted (" + output + ")");
        }

        return output;
    }

    public void signAndEncryptAsync(Intent data, String input, String[] userIds) {
        data.setAction(OpenPgpApi.ACTION_SIGN_AND_ENCRYPT);
        data.putExtra(OpenPgpApi.EXTRA_SIGN_KEY_ID, mSignKeyId);
        data.putExtra(OpenPgpApi.EXTRA_USER_IDS, userIds);
        data.putExtra(OpenPgpApi.EXTRA_REQUEST_ASCII_ARMOR, true);
        data.putExtra(OpenPgpApi.EXTRA_PASSPHRASE, mSignKeyPassphrase.toCharArray());

        InputStream is = getInputstream(input);
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        OpenPgpApi api = new OpenPgpApi(mContext, mServiceConnection.getService());
        api.executeApiAsync(data, is, os, new MyCallback(true, os, REQUEST_CODE_SIGN_AND_ENCRYPT));
    }

    public String signAndEncrypt(Intent data, String input, Contact to) {
        Log.i(TAG, "OpenPGPID: " + to.getOpenPGPID());
        data.setAction(OpenPgpApi.ACTION_SIGN_AND_ENCRYPT);
        data.putExtra(OpenPgpApi.EXTRA_SIGN_KEY_ID, mSignKeyId);
        data.putExtra(OpenPgpApi.EXTRA_USER_IDS, new String[]{to.getOpenPGPID()});
        data.putExtra(OpenPgpApi.EXTRA_REQUEST_ASCII_ARMOR, true);
        data.putExtra(OpenPgpApi.EXTRA_PASSPHRASE, mSignKeyPassphrase.toCharArray());

        InputStream is = getInputstream(input);
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        OpenPgpApi api = new OpenPgpApi(mContext, mServiceConnection.getService());
        api.executeApi(data, is, os);

        String output = "";
        try {
            output = os.toString("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (output.isEmpty()) {
            Log.e(Constants.TAG, "message couldn't be signed and encrypted");
        } else {
            Log.i(Constants.TAG, "message encrypted and signed (" + output + ")");
        }

        return output;
    }

    public void decryptAndVerifyAsync(Intent data, String input) {
        data.setAction(OpenPgpApi.ACTION_DECRYPT_VERIFY);
        data.putExtra(OpenPgpApi.EXTRA_PASSPHRASE, mSignKeyPassphrase.toCharArray());

        InputStream is = getInputstream(input);
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        OpenPgpApi api = new OpenPgpApi(mContext, mServiceConnection.getService());
        api.executeApiAsync(data, is, os, new MyCallback(false, os, REQUEST_CODE_DECRYPT_AND_VERIFY));
    }

    public String decryptAndVerify(Intent data, String input) {
        data.setAction(OpenPgpApi.ACTION_DECRYPT_VERIFY);
        data.putExtra(OpenPgpApi.EXTRA_PASSPHRASE, mSignKeyPassphrase.toCharArray());

        InputStream is = getInputstream(input);
        ByteArrayOutputStream os = new ByteArrayOutputStream();


        OpenPgpApi api = new OpenPgpApi(mContext, mServiceConnection.getService());
        api.executeApi(data, is, os);

        String output = "";
        try {
            output = os.toString("UTF-8");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Log.i(Constants.TAG, "message verified and decrypted (" + output + ")");
        return output;
    }

    public void getKeyIdsAsync(Intent data, String[] user_ids) {
        data.setAction(OpenPgpApi.ACTION_GET_KEY_IDS);
        data.putExtra(OpenPgpApi.EXTRA_USER_IDS, user_ids);

        OpenPgpApi api = new OpenPgpApi(mContext, mServiceConnection.getService());
        api.executeApiAsync(data, null, null, new MyCallback(false, null, REQUEST_CODE_GET_KEY_IDS));
    }

    /**
     * checks if there is a usable key for the user
     * dirty hack!
     */
    public boolean isSecureChannelForUserAvailable(Contact contact) {
        if (TextUtils.isEmpty(providerPackageName)) {
            Log.e(Constants.TAG, "no Open PGP Provider");
            return false;
        }

        String os = signAndEncrypt(new Intent(), "...", contact);

        if (os.isEmpty()) {
            Log.e(TAG, "can't encrypt messages for user with OpenPGPID " + contact.getOpenPGPID());
            return false;
        }
        return true;
    }


    public void getAnyKeyIds(Intent data) {
        data.setAction(OpenPgpApi.ACTION_GET_KEY_IDS);

        OpenPgpApi api = new OpenPgpApi(mContext, mServiceConnection.getService());
        api.executeApiAsync(data, null, null, new MyCallback(false, null, REQUEST_CODE_GET_KEY_IDS));
    }

    @Override
    public void onBound(IOpenPgpService2 service) {
        Log.i("PGP", "ISBOUND!!");


    }

    @Override
    public void onError(Exception e) {

    }

}
