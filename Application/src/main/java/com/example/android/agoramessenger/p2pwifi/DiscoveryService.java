package com.example.android.agoramessenger.p2pwifi;

/*
 * Copyright (c) 2014 Albert C. Braun
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.app.Service;
import android.content.Intent;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.android.agoramessenger.Constants;


class DiscoveryService extends Service {

    private static final String TAG = "DiscoveryService";

    private WifiP2pManager wifiP2pManager = null;
    private WifiP2pManager.Channel wifiP2pChannel = null;

    /**
     * Main constructor.
     *
     * @param wifiP2pManager current, initialized instance of the Android SDK's {@link WifiP2pManager}
     * @param wifiP2pChannel instance of the {@link WifiP2pManager.Channel} object associated with the manager
     */
    DiscoveryService(WifiP2pManager wifiP2pManager, WifiP2pManager.Channel wifiP2pChannel) {
        this.wifiP2pManager = wifiP2pManager;
        this.wifiP2pChannel = wifiP2pChannel;
    }


    public void run() {
        while(true) {
            wifiP2pManager.discoverPeers(wifiP2pChannel, new WifiP2pManager.ActionListener() {
                @Override
                public void onSuccess() {
                    Log.v(TAG, "successful discovered peers.");
                }

                @Override
                public void onFailure(int reasonCode) {
                    Log.e(TAG, "unsuccessful discovered peers.");
                }
            });

            try {
                Thread.sleep(Constants.WIFI_DISCOVERY_PERIOD);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
