/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.agoramessenger.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Handler;

import com.example.android.agoramessenger.MainApp;
import com.example.android.agoramessenger.Utils;
import com.example.android.agoramessenger.protocol.ProtocolHandler;
import com.example.android.agoramessenger.protocol.database.TransmissionMessage;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.UUID;

/**
 * This class does all the work for setting up and managing Bluetooth
 * connections with other devices. It has a thread that listens for
 * incoming connections, a thread for connecting with a device, and a
 * thread for performing data transmissions when connected.
 */
public class BluetoothChatService {

    private static BluetoothChatService mBluetoothChatService;

    // Debugging
    private static final String TAG = "BluetoothChatService";

    // Name for the SDP record when creating server socket
    private static final String NAME_SECURE = "BluetoothChatSecure";
    private static final String NAME_INSECURE = "BluetoothChatInsecure";

    // Unique UUID for this application
    private static final UUID MY_UUID_SECURE =
            UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");
    private static final UUID MY_UUID_INSECURE =
            UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");

    // Member fields
    private final BluetoothAdapter mAdapter;
    private Handler mHandler;
    private AcceptThread mSecureAcceptThread;
    private AcceptThread mInsecureAcceptThread;
    private ConnectThread mConnectThread;
    private ConnectedThreadRead mConnectedThreadRead;
    private ConnectedThreadWrite mConnectedThreadWrite;
    private DiscoveryThread mDiscoveryThread;
    private int mState;
    private boolean mNoMoreToSend;
    private boolean mNoMoreToRead;

    private Context mContext;
    private ProtocolHandler mProtocolHandler;

    // Constants that indicate the current connection state
    public static final int STATE_NONE = 0;       // we're doing nothing
    public static final int STATE_LISTEN = 1;     // now listening for incoming connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
    public static final int STATE_CONNECTED = 3;  // now connected to a remote device
    public static final byte END_OF_MESSAGE = '.';

    private boolean stopped = false;


    public static BluetoothChatService getInstance(Context context, Handler handler) {
        if (mBluetoothChatService == null) {
            mBluetoothChatService = new BluetoothChatService(context, handler);
        }

        if (handler != null) {
            mBluetoothChatService.mHandler = handler;
        }

        return mBluetoothChatService;
    }

    public void stopService() {
        stopped = true;
        stop();
    }

    public void startService() {
        stopped = false;
        start();
    }

    /**
     * Constructor. Prepares a new BluetoothChat session.
     *
     * @param context The UI Activity Context
     */
    public BluetoothChatService(Context context, Handler handler) {
        mContext = context;
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        mState = STATE_NONE;
        mHandler = handler;
        mProtocolHandler = new ProtocolHandler(context);
    }

    /**
     * Set the current state of the chat connection
     *
     * @param state An integer defining the current connection state
     */
    private synchronized void setState(int state) {
        Log.d(TAG, "setState() " + mState + " -> " + state);
        mState = state;
    }

    /**
     * Return the current connection state.
     */
    public synchronized int getState() {
        return mState;
    }

    /**
     * Start the chat service. Specifically start AcceptThread to begin a
     * session in listening (server) mode. Called by the Activity onResume()
     */
    public synchronized void start() {
        Log.d(TAG, "start");

        // Cancel any thread attempting to make a connection
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }


        // Cancel any thread currently running a connection
        if (mConnectedThreadRead != null) {
            mConnectedThreadRead.cancel();
            mConnectedThreadRead = null;
        }

        // Cancel any thread currently running a connection
        if (mConnectedThreadWrite != null) {
            mConnectedThreadWrite.cancel();
            mConnectedThreadWrite = null;
        }

        setState(STATE_LISTEN);

        if (mInsecureAcceptThread == null) {
            mInsecureAcceptThread = new AcceptThread(false);
            mInsecureAcceptThread.start();
        }

        if (mDiscoveryThread == null) {
            mDiscoveryThread = new DiscoveryThread(32);
            mDiscoveryThread.start();
        }

    }

    /**
     * start discovering bt devices q
     */
    public synchronized void startDiscovering() {
        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        btAdapter.cancelDiscovery();

        if (!btAdapter.isDiscovering()) {
            btAdapter.startDiscovery();
            Log.i(TAG, "start discovering bluetooth-devices");
        }
    }

    /**
     * cancel discovering bt devices
     */
    public synchronized void cancelDiscovering() {
        // cancel discovery thread
        if (mDiscoveryThread != null) {
            mDiscoveryThread.cancel();
            mDiscoveryThread = null;
        }

    }

    /**
     * Start the ConnectThread to initiate a connection to a remote device.
     *
     * @param device The BluetoothDevice to connect
     * @param secure Socket Security type - Secure (true) , Insecure (false)
     */
    public synchronized void connect(BluetoothDevice device, boolean secure) {
        // there is data to read and to send

        Log.d(TAG, "connect to: " + device);

        // Cancel any thread attempting to make a connection
        if (mState == STATE_CONNECTING) {
            if (mConnectThread != null) {
                mConnectThread.cancel();
                mConnectThread = null;
            }
        }

        // Cancel any thread currently running a connection
        if (mConnectedThreadRead != null) {
            mConnectedThreadRead.cancel();
            mConnectedThreadRead = null;
        }

        // Cancel any thread currently running a connection
        if (mConnectedThreadWrite != null) {
            mConnectedThreadWrite.cancel();
            mConnectedThreadWrite = null;
        }

        cancelDiscovering();

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Start the thread to connect with the given device
        mConnectThread = new ConnectThread(device, secure);
        mConnectThread.start();
        setState(STATE_CONNECTING);
    }


    /**
     * stops all threads
     */
    public synchronized void disconnect() {
        // Cancel any thread attempting to make a connection
        if (mState == STATE_CONNECTING) {
            if (mConnectThread != null) {
                mConnectThread.cancel();
                mConnectThread = null;
            }
        }
        // Cancel any thread currently running a connection
        if (mConnectedThreadRead != null) {
            mConnectedThreadRead.cancel();
            mConnectedThreadRead = null;
        }

        // Cancel any thread currently running a connection
        if (mConnectedThreadWrite != null) {
            mConnectedThreadWrite.cancel();
            mConnectedThreadWrite = null;
        }


        cancelDiscovering();

    }


    /**
     * Start the ConnectedThread to begin managing a Bluetooth connection
     *
     * @param socket The BluetoothSocket on which the connection was made
     * @param device The BluetoothDevice that has been connected
     */
    public synchronized void connected(BluetoothSocket socket, BluetoothDevice
            device, final String socketType) {
        Log.d(TAG, "connected, Socket Type:" + socketType);

        // Cancel the thread that completed the connection
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        // Cancel any thread currently running a connection
        if (mConnectedThreadRead != null) {
            mConnectedThreadRead.cancel();
            mConnectedThreadRead = null;
        }

        // Cancel any thread currently running a connection
        if (mConnectedThreadWrite != null) {
            mConnectedThreadWrite.cancel();
            mConnectedThreadWrite = null;
        }

        // Cancel the accept thread because we only want to connect to one device
        if (mSecureAcceptThread != null) {
            mSecureAcceptThread.cancel();
            mSecureAcceptThread = null;
        }
        if (mInsecureAcceptThread != null) {
            mInsecureAcceptThread.cancel();
            mInsecureAcceptThread = null;
        }

        cancelDiscovering();

        mNoMoreToRead = false;
        mNoMoreToSend = false;

        // Start the thread to manage the connection and perform transmissions
        mConnectedThreadRead = new ConnectedThreadRead(socket, socketType, device);
        mConnectedThreadRead.start();

        mConnectedThreadWrite = new ConnectedThreadWrite(socket, socketType, device);
        mConnectedThreadWrite.start();

        ProtocolHandler protocolHelper = new ProtocolHandler(mContext, mHandler);


        setState(STATE_CONNECTED);
    }


    /**
     * Stop all threads
     */
    public synchronized void stop() {
        Log.d(TAG, "stop");

        cancelDiscovering();

        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        // Cancel any thread currently running a connection
        if (mConnectedThreadRead != null) {
            mConnectedThreadRead.cancel();
            mConnectedThreadRead = null;
        }

        // Cancel any thread currently running a connection
        if (mConnectedThreadWrite != null) {
            mConnectedThreadWrite.cancel();
            mConnectedThreadWrite = null;
        }

        if (mSecureAcceptThread != null) {
            mSecureAcceptThread.cancel();
            mSecureAcceptThread = null;
        }

        if (mInsecureAcceptThread != null) {
            mInsecureAcceptThread.cancel();
            mInsecureAcceptThread = null;
        }

        // cancel discovering new btdevices
        if (mDiscoveryThread != null) {
            mDiscoveryThread.cancel();
            mDiscoveryThread = null;
        }

        // ToDo
        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        btAdapter.cancelDiscovery();


        setState(STATE_NONE);
    }

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     *
     * @param out The bytes to write
     * @see ConnectedThreadWrite#write(byte[])
     */
    public void write(byte[] out) {
        // Create temporary object
        ConnectedThreadWrite r;
        // Synchronize a copy of the ConnectedThread
        synchronized (this) {
            if (mState != STATE_CONNECTED) return;
            r = mConnectedThreadWrite;
        }
        // Perform the write unsynchronized
        r.write(out);
    }

    /**
     * Indicate that the connection attempt failed and notify the UI Activity.
     */
    private void connectionFailed() {
        // Start the service over to restart listening mode
        BluetoothChatService.this.start();
    }

    /**
     * Indicate that the connection was lost and notify the UI Activity.
     */
    private void connectionLost() {
        // Start the service over to restart listening mode
        BluetoothChatService.this.start();
    }


    /**
     * This thread runs while listening for incoming connections. It behaves
     * like a server-side client. It runs until a connection is accepted
     * (or until cancelled).
     */
    private class AcceptThread extends Thread {
        // The local server socket
        private final BluetoothServerSocket mmServerSocket;
        private String mSocketType;

        public AcceptThread(boolean secure) {
            BluetoothServerSocket tmp = null;
            mSocketType = secure ? "Secure" : "Insecure";

            // Create a new listening server socket
            try {
                if (secure) {
                    tmp = mAdapter.listenUsingRfcommWithServiceRecord(NAME_SECURE,
                            MY_UUID_SECURE);
                } else {
                    tmp = mAdapter.listenUsingInsecureRfcommWithServiceRecord(
                            NAME_INSECURE, MY_UUID_INSECURE);
                }
            } catch (IOException e) {
                Log.e(TAG, "Socket Type: " + mSocketType + "listen() failed", e);
            }
            mmServerSocket = tmp;
        }

        public void run() {
            // check if service is stopped
            if (mBluetoothChatService.stopped) {
                return;
            }

            Log.d(TAG, "Socket Type: " + mSocketType +
                    "BEGIN mAcceptThread" + this);
            setName("AcceptThread" + mSocketType);

            BluetoothSocket socket = null;

            // Listen to the server socket if we're not connected
            while (mState != STATE_CONNECTED) {
                try {
                    // This is a blocking call and will only return on a
                    // successful connection or an exception
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    Log.e(TAG, "Socket Type: " + mSocketType + " accept() failed", e);
                    break;
                }

                // If a connection was accepted
                if (socket != null) {
                    cancelDiscovering();

                    synchronized (BluetoothChatService.this) {
                        switch (mState) {
                            case STATE_LISTEN:
                            case STATE_CONNECTING:
                                // Situation normal. Start the connected thread.
                                connected(socket, socket.getRemoteDevice(),
                                        mSocketType);
                                break;
                            case STATE_NONE:
                            case STATE_CONNECTED:
                                // Either not ready or already connected. Terminate new socket.
                                try {
                                    socket.close();
                                } catch (IOException e) {
                                    Log.e(TAG, "Could not close unwanted socket", e);
                                }
                                break;
                        }
                    }
                }
            }
            Log.i(TAG, "END mAcceptThread, socket Type: " + mSocketType);

        }

        public void cancel() {
            Log.d(TAG, "Socket Type" + mSocketType + "cancel " + this);
            try {
                mmServerSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Socket Type" + mSocketType + "close() of server failed", e);
            }
        }
    }


    /**
     * This thread starts bluetooth discovering every x seconds
     */
    public class DiscoveryThread extends Thread {
        int seconds;

        public DiscoveryThread(int seconds) {
            this.seconds = 30;
        }

        public void run() {

            while (true) {
                // check if service is stopped
                if (mBluetoothChatService.stopped) {
                    return;
                }

                try {
//                    Log.e(TAG, "try to start discovering");
                    startDiscovering();

                    Thread.sleep(1000 * seconds);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }

        public void cancel() {
            BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
            if (btAdapter.isDiscovering()) {
                Log.i(TAG, "cancel discovering bluetooth-devices");
                btAdapter.cancelDiscovery();
            }
        }
    }


    /**
     * This thread runs while attempting to make an outgoing connection
     * with a device. It runs straight through; the connection either
     * succeeds or fails.
     */
    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;
        private String mSocketType;

        public ConnectThread(BluetoothDevice device, boolean secure) {
            mmDevice = device;
            BluetoothSocket tmp = null;
            mSocketType = secure ? "Secure" : "Insecure";

            // Get a BluetoothSocket for a connection with the
            // given BluetoothDevice
            try {
                if (secure) {
                    tmp = device.createRfcommSocketToServiceRecord(
                            MY_UUID_SECURE);
                } else {
                    tmp = device.createInsecureRfcommSocketToServiceRecord(
                            MY_UUID_INSECURE);
                }
            } catch (IOException e) {
                Log.e(TAG, "Socket Type: " + mSocketType + "create() failed", e);
            }
            mmSocket = tmp;
            mState = STATE_CONNECTING;
        }

        public void run() {
            // check if service is stopped
            if (mBluetoothChatService.stopped) {
                return;
            }

            Log.i(TAG, "BEGIN mConnectThread SocketType:" + mSocketType);
            setName("ConnectThread" + mSocketType);

            // Always cancel discovery because it will slow down a connection
            mAdapter.cancelDiscovery();

            // Make a connection to the BluetoothSocket
            try {
                mmSocket.connect();
            } catch (IOException e) {
                Log.e(TAG, "error: " + e);
                // Close the socket
                try {
                    mmSocket.close();
                } catch (IOException e2) {
                    Log.e(TAG, "unable to close() " + mSocketType +
                            " socket during connection failure", e2);
                }
                connectionFailed();
                return;
            }

            // Reset the ConnectThread because we're done
            synchronized (BluetoothChatService.this) {
                mConnectThread = null;
            }

            // Start the connected thread
            connected(mmSocket, mmDevice, mSocketType);
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect " + mSocketType + " socket failed", e);
            }
        }
    }

    /**
     * This thread runs during a connection with a remote device.
     * It handles all incoming and outgoing transmissions.
     */
    private class ConnectedThreadRead extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private BluetoothDevice mDevice;

        //        private final OutputStream mmOutStream;
        ProtocolHandler protocolHelper;

        public ConnectedThreadRead(BluetoothSocket socket, String socketType, BluetoothDevice device) {
            Log.d(TAG, "create ConnectedThread: " + socketType);
            mmSocket = socket;
            InputStream tmpIn = null;

            // Get the BluetoothSocket input and output streams
            try {
                tmpIn = socket.getInputStream();
            } catch (IOException e) {
                Log.e(TAG, "temp sockets not created", e);
            }

            mDevice = device;

            mmInStream = tmpIn;

            mState = STATE_CONNECTED;
        }


        public void run() {
            // check if service is stopped
            if (mBluetoothChatService.stopped) {
                return;
            }

            Log.i(TAG, "BEGIN mConnectedThreadRead");

            protocolHelper = new ProtocolHandler(MainApp.context, mHandler);

            byte[] buffer = new byte[1024];
            int bytes = 0;

            String messages_json = "";

            try {
                while ((bytes = mmInStream.read(buffer)) >= 0) {
                    messages_json += new String(buffer, 0, bytes);
                    Log.d(TAG, "buffer: " + messages_json);
                }

            } catch (IOException e) {
                Log.e(TAG, "disconnected: " + e, e);

            }

            Log.d(TAG, "received: " + messages_json);

            List<TransmissionMessage> messages = Utils.jsonToMessages(messages_json);

            protocolHelper.processMessages(messages);

            protocolHelper.connectToDevice(mDevice.getAddress());


            Log.d(TAG, "messages received: " + messages.size());

            mNoMoreToRead = true;


            while (!(mNoMoreToRead && mNoMoreToSend)) {

            }

            Log.d(TAG, "close socket (read)!");

            cancel();
            BluetoothChatService.this.start();
        }

        public void cancel() {
            try {
                Log.e(TAG, "connection to bt device canceled");
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect socket failed", e);
            }
        }
    }

    /**
     * This thread runs during a connection with a remote device.
     * It handles all incoming and outgoing transmissions.
     */
    private class ConnectedThreadWrite extends Thread {
        private final BluetoothSocket mmSocket;
        //            private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        ProtocolHandler protocolHelper;

        private BluetoothDevice mDevice;
        private List<TransmissionMessage> messages;

        public ConnectedThreadWrite(BluetoothSocket socket, String socketType, BluetoothDevice device) {
            Log.d(TAG, "create ConnectedThread: " + socketType);

            mmSocket = socket;
            mDevice = device;
            OutputStream tmpOut = null;

            // Get the BluetoothSocket input and output streams
            try {
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "temp sockets not created", e);
            }

//                mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            // check if service is stopped
            if (mBluetoothChatService.stopped) {
                return;
            }

            Log.i(TAG, "BEGIN ConnectedThreadWrite");

            protocolHelper = new ProtocolHandler(MainApp.context, mHandler);

            messages = protocolHelper.getMessagesForDevice(mDevice.getAddress());
            Log.d(TAG, "Messages to send: " + messages.size());

            String messages_json = Utils.messagesToJson(messages);

            BluetoothChatService.this.write(messages_json.getBytes());

            Log.d(TAG, "all messanges send.");

            mNoMoreToSend = true;

            while (!(mNoMoreToRead && mNoMoreToSend)) {

            }

            Log.d(TAG, "close socket (write)!");

            cancel();
            BluetoothChatService.this.start();
        }

        /**
         * Write to the connected OutStream.
         *
         * @param buffer The bytes to write
         */
        public void write(byte[] buffer) {
            try {
                mmOutStream.write(buffer);
            } catch (IOException e) {
                Log.e(TAG, "Exception during write", e);
            }
        }

        public void cancel() {
            try {
                Log.e(TAG, "connection to bt device canceled");
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect socket failed", e);
            }
        }
    }
}