package com.example.android.agoramessenger.p2pwifi;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.ChannelListener;
import android.net.wifi.p2p.WifiP2pManager.ConnectionInfoListener;
import android.net.wifi.p2p.WifiP2pManager.PeerListListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;

public class ConnectionService extends Service implements ChannelListener, PeerListListener, ConnectionInfoListener {  // callback of requestPeers{

    private static final String TAG = "ConnectionService";

    private static ConnectionService _sinstance = null;

    private  WorkHandler mWorkHandler;
    private  MessageHandler mHandler;

    boolean retryChannel = false;
    public WifiP2pManager mP2pMan;
    public WifiP2pManager.Channel mP2pChannel;
    ConnectionManager mConnMan;
    private WifiP2pDevice mThisDevice;
    List<WifiP2pDevice> mPeers = new ArrayList<WifiP2pDevice>();  // update on every peers available
    boolean mP2pConnected = false;
    WifiP2pInfo mP2pInfo = null;  // set when connection info available, reset when WIFI_P2P_CONNECTION_CHANGED_ACTION
    String mDeviceName = null;   // the p2p name that is configurated from UI.
    boolean mIsServer = false;
    String mMyAddr = null;

    /**
     * @see android.app.Service#onCreate()
     */
    private void _initialize() {
        if (_sinstance != null) {
            Log.d(TAG, "_initialize, already initialized, do nothing.");
            return;
        }

        Log.d(TAG, "_initialize, initialized.");

        _sinstance = this;

        mWorkHandler = new WorkHandler(TAG);
        mHandler = new MessageHandler(mWorkHandler.getLooper());

        mP2pMan = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        mP2pChannel = mP2pMan.initialize(this, mWorkHandler.getLooper(), null);

        Log.d(TAG, "_initialize, get p2p service and init channel !!!");

        mConnMan = new ConnectionManager(this);
    }

    public static ConnectionService getInstance(){
        return _sinstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        _initialize();

        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        WiFiDirectBroadcastReceiver receiver = new WiFiDirectBroadcastReceiver();
        registerReceiver(receiver, mIntentFilter);

        // discover Peers for the first time
        discoverPeers();
    }

    public void discoverPeers() {
        mP2pMan.discoverPeers(mP2pChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "onOptionsItemSelected : discovery succeed... " );
            }

            @Override
            public void onFailure(int reasonCode) {
                Log.d(TAG, "onOptionsItemSelected : discovery failed !!! " + reasonCode);
            }
        });
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        _initialize();
        processIntent(intent);
        return START_STICKY;
    }

    /**
     * process all wifi p2p intent caught by bcast recver.
     * P2P connection setup event sequence:
     * 1. after find, peers_changed to available, invited
     * 2. when connection established, this device changed to connected.
     * 3. for server, WIFI_P2P_CONNECTION_CHANGED_ACTION intent: p2p connected,
     *    for client, this device changed to connected first, then CONNECTION_CHANGED
     * 4. WIFI_P2P_PEERS_CHANGED_ACTION: peer changed to connected.
     * 5. now both this device and peer are connected !
     *
     * if select p2p server mode with create group, this device will be group owner automatically, with
     * 1. this device changed to connected
     * 2. WIFI_P2P_CONNECTION_CHANGED_ACTION
     */
    private void processIntent(Intent intent){
        if( intent == null){
            return;
        }

        String action = intent.getAction();
        Log.d(TAG, "processIntent: " + intent.toString());

        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {  // this devices's wifi direct enabled state.
            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
            if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                // Wifi Direct mode is enabled
                mP2pChannel = mP2pMan.initialize(this, mWorkHandler.getLooper(), null);
//                AppPreferences.setStringToPref(mApp, AppPreferences.PREF_NAME, AppPreferences.P2P_ENABLED, "1");
                Log.d(TAG, "processIntent : WIFI_P2P_STATE_CHANGED_ACTION : enabled, re-init p2p channel to framework ");
            } else {
                mThisDevice = null;  	// reset this device status
                mP2pChannel = null;
                mPeers.clear();
                Log.d(TAG, "processIntent : WIFI_P2P_STATE_CHANGED_ACTION : disabled, null p2p channel to framework ");

//                AppPreferences.setStringToPref(mApp, AppPreferences.PREF_NAME, AppPreferences.P2P_ENABLED, "0");
            }

        } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
            // a list of peers are available after discovery, use PeerListListener to collect
            // request available peers from the wifi p2p manager. This is an
            // asynchronous call and the calling activity is notified with a
            // callback on PeerListListener.onPeersAvailable()
            Log.d(TAG, "processIntent: WIFI_P2P_PEERS_CHANGED_ACTION: call requestPeers() to get list of peers");
            if (mP2pMan != null) {
                mP2pMan.requestPeers(mP2pChannel, (PeerListListener) this);
            }

        } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
            NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
            Log.d(TAG, "processIntent: WIFI_P2P_CONNECTION_CHANGED_ACTION : " + networkInfo.getReason() + " : " + networkInfo.toString());

            if (mP2pMan == null) {
                Log.e(TAG, "Error: connection changed action, but WifiP2pManager is null");
                return;
            }

            if (networkInfo.isConnected()) {
                Log.d(TAG, "processIntent: WIFI_P2P_CONNECTION_CHANGED_ACTION: p2p connected ");
                // Connected with the other device, request connection info for group owner IP. Callback inside details fragment.
                mP2pMan.requestConnectionInfo(mP2pChannel, this);
            } else {
                Log.d(TAG, "processIntent: WIFI_P2P_CONNECTION_CHANGED_ACTION: p2p disconnected, mP2pConnected = false..closeClient.."); // It's a disconnect
                mP2pConnected = false;
                mP2pInfo = null;   // reset connection info after connection done.
                mConnMan.closeClient();
            }

        } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
            // this device details has changed(name, connected, etc)
            mThisDevice = (WifiP2pDevice) intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE);
            mDeviceName = mThisDevice.deviceName;
            Log.d(TAG, "processIntent: WIFI_P2P_THIS_DEVICE_CHANGED_ACTION " + mThisDevice.deviceName);

        }
    }

    /**
     * The channel to the framework Wifi P2p has been disconnected. could try re-initializing
     */
    @Override
    public void onChannelDisconnected() {
        if( !retryChannel ){
            Log.d(TAG, "onChannelDisconnected : retry initialize() ");
            mP2pChannel = mP2pMan.initialize(this, mWorkHandler.getLooper(), null);

            retryChannel = true;
        }else{
            Log.d(TAG, "onChannelDisconnected : stoped");

            stopSelf();
        }
    }

    /**
     * the callback of requestPeers upon WIFI_P2P_PEERS_CHANGED_ACTION intent.
     */
    @Override
    public void onPeersAvailable(WifiP2pDeviceList peerList) {
        mPeers.clear();
        mPeers.addAll(peerList.getDeviceList());
        Log.d(TAG, "onPeersAvailable : update peer list...");

        WifiP2pDevice connectedPeer = getConnectedPeer();
        if(connectedPeer != null){
            Log.d(TAG, "onPeersAvailable : connected peer exists: " + connectedPeer.deviceName);
        } else {

        }

        if(mP2pInfo != null && connectedPeer != null ){
            // start server if device is group owner
            if( mP2pInfo.groupFormed && mP2pInfo.isGroupOwner ){
                Log.d(TAG, "onPeersAvailable : device is groupOwner: startSocketServer");
                startSocketServer();

            } else if( mP2pInfo.groupFormed && connectedPeer != null ){
                // client path goes to connection info available after connection established.
                // Log.d(TAG, "onConnectionInfoAvailable: device is client, connect to group owner: startSocketClient ");
                // mApp.startSocketClient(mApp.mP2pInfo.groupOwnerAddress.getHostAddress());
            }
        }

        // if peerList is not empty, connect to first peer
        if(connectedPeer == null && !mPeers.isEmpty()) {
            WifiP2pDevice device = mPeers.get(0);
            WifiP2pConfig config = new WifiP2pConfig();
            config.deviceAddress = device.deviceAddress;
            config.wps.setup = WpsInfo.PBC;
            config.groupOwnerIntent = 0;  // least inclination to be group owner.

            Log.d(TAG, "connect : connect to server with address: " + config.deviceAddress);

            // perform p2p connect upon users click the connect button. after connection, manager request connection info.
            mP2pMan.connect(mP2pChannel, config, new WifiP2pManager.ActionListener() {
                @Override
                public void onSuccess() {
                    // WiFiDirectBroadcastReceiver will notify us. Ignore for now.
                    Log.d(TAG, "Connect success..");
                }
                @Override
                public void onFailure(int reason) {
                    Log.d(TAG, "Connect failed.Retry.");
                }
            });
        }

    }



    /**
     * check whether there exists a connected peer.
     */
    public WifiP2pDevice getConnectedPeer(){
        WifiP2pDevice peer = null;
        for(WifiP2pDevice d : mPeers ){
            Log.d(TAG, "getConnectedPeer : device : " + d.deviceName + " status: " + ConnectionService.getDeviceStatus(d.status));
            if( d.status == WifiP2pDevice.CONNECTED){
                peer = d;
            }
        }
        return peer;
    }

    /**
     * upon p2p connection available, group owner start server socket channel
     * start socket server and select monitor the socket
     */
    public void startSocketServer() {
        Message msg = ConnectionService.getInstance().getHandler().obtainMessage();
        msg.what = Constants.MSG_STARTSERVER;
        ConnectionService.getInstance().getHandler().sendMessage(msg);
    }

    /**
     * upon p2p connection available, non group owner start socket channel connect to group owner.
     */
    public void startSocketClient(String hostname) {
        Log.d(TAG, "startSocketClient : client connect to group owner : " + hostname);
        Message msg = ConnectionService.getInstance().getHandler().obtainMessage();
        msg.what = Constants.MSG_STARTCLIENT;
        msg.obj = hostname;
        ConnectionService.getInstance().getHandler().sendMessage(msg);
    }

    /**
     * the callback of when the _Requested_ connectino info is available.
     * WIFI_P2P_CONNECTION_CHANGED_ACTION intent, requestConnectionInfo()
     */
    @Override
    public void onConnectionInfoAvailable(final WifiP2pInfo info) {
        Log.d(TAG, "onConnectionInfoAvailable: " + info.groupOwnerAddress.getHostAddress());
        if (info.groupFormed && info.isGroupOwner ) {
            // XXX server path goes to peer connected.
            //new FileServerAsyncTask(getActivity(), mContentView.findViewById(R.id.status_text)).execute();
            // Log.d(TAG, "onConnectionInfoAvailable: device is groupOwner: startSocketServer ");
            //  startSocketServer();
        } else if (info.groupFormed) {
            Log.d(TAG, "onConnectionInfoAvailable: device is client, connect to group owner: startSocketClient ");
            startSocketClient(info.groupOwnerAddress.getHostAddress());
        }
        mP2pConnected = true;
        mP2pInfo = info;   // connection info available
    }


    @Override
    public IBinder onBind(Intent arg0) { return null; }

    public Handler getHandler() {
        return mHandler;
    }

    /**
     * message handler looper to handle all the msg sent to location manager.
     */
    final class MessageHandler extends Handler {
        public MessageHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            processMessage(msg);
        }
    }

    /**
     * p2p connection setup, proceed to setup socket connection.
     */
    public void connect(final WifiP2pInfo info) {

        Log.d(TAG, "onConnectionInfoAvailable: " + info.groupOwnerAddress.getHostAddress());
        if (info.groupFormed && info.isGroupOwner) {
            //new FileServerAsyncTask(getActivity(), mContentView.findViewById(R.id.status_text)).execute();
            Log.d(TAG, "onConnectionInfoAvailable: device is groupOwner: startSocketServer done ");
        } else if (info.groupFormed) {
            Log.d(TAG, "onConnectionInfoAvailable: device is client, connect to group owner: startSocketClient done ");
        }

        if( ! mIsServer && mMyAddr == null ){
            Log.d(TAG, "onConnectionInfoAvailable : connect to serve failed...try again ! ");
        }else{
            // hide the connect button and enable start chat button
            Log.d(TAG, "onConnectionInfoAvailable: socket connection established, show start chat button ! ");
        }
    }

    /**
     * the main message process loop.
     */
    private void processMessage(android.os.Message msg) {

        switch (msg.what) {
            case Constants.MSG_NULL:
                break;
            case Constants.MSG_STARTSERVER:
                Log.d(TAG, "processMessage: startServerSelector...");
                if( mConnMan.startServer() >= 0){
                    connect(mP2pInfo);
                }
                break;
            case Constants.MSG_STARTCLIENT:
                Log.d(TAG, "processMessage: startClientSelector...");
                if( mConnMan.startClient((String)msg.obj) >= 0){
                    connect(mP2pInfo);
                }
                break;
            case Constants.MSG_NEW_CLIENT:
                Log.d(TAG, "processMessage:  onNewClient...");
                mConnMan.onNewClient((SocketChannel)msg.obj);
                break;
            case Constants.MSG_FINISH_CONNECT:
                Log.d(TAG, "processMessage:  onFinishConnect...");
                mConnMan.onFinishConnect((SocketChannel)msg.obj);
                break;
            case Constants.MSG_PULLIN_DATA:
                Log.d(TAG, "processMessage:  onPullIndata ...");
                onPullInData((SocketChannel)msg.obj, msg.getData());
                break;
            case Constants.MSG_PUSHOUT_DATA:
                Log.d(TAG, "processMessage: onPushOutData...");
                onPushOutData((String)msg.obj);
                break;
            case Constants.MSG_SELECT_ERROR:
                Log.d(TAG, "processMessage: onSelectorError...");
                mConnMan.onSelectorError();
                break;
            case Constants.MSG_BROKEN_CONN:
                Log.d(TAG, "processMessage: onBrokenConn...");
                mConnMan.onBrokenConn((SocketChannel)msg.obj);
                break;
            default:
                break;
        }
    }

    public void setMyAddr(String addr){
        mMyAddr = addr;
    }

    /**
     * service handle data in come from socket channel
     */
    private String onPullInData(SocketChannel schannel, Bundle b){

        String data = b.getString("DATA");
        Log.d(TAG, "onDataIn : recvd msg : " + data);

        return data;
    }

    /**
     * handle data push out request.
     * If the sender is the server, pub to all client.
     * If the sender is client, only can send to the server.
     */
    private void onPushOutData(String data){
        Log.d(TAG, "onPushOutData : " + data);
        mConnMan.pushOutData(data);
    }

    /**
     * sync call to send data using conn man's channel, as conn man now is blocking on select
     */
    public int connectionSendData(String jsonstring) {
        Log.d(TAG, "connectionSendData : " + jsonstring);
        new SendDataAsyncTask(mConnMan, jsonstring).execute();
        return 0;
    }

    /**
     * write data in an async task to avoid NetworkOnMainThreadException.
     */
    public class SendDataAsyncTask extends AsyncTask<Void, Void, Integer> {
        private String data;
        private ConnectionManager connman;

        public SendDataAsyncTask(ConnectionManager conn, String jsonstring) {
            connman = conn;
            data = jsonstring;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            return connman.pushOutData(data);
        }

        @Override
        protected void onPostExecute(Integer result) {
            Log.d(TAG, "SendDataAsyncTask : onPostExecute:  " + data + " len: " + result);
        }
    }





    public static String getDeviceStatus(int deviceStatus) {
        switch (deviceStatus) {
            case WifiP2pDevice.AVAILABLE:
                //Log.d(TAG, "getDeviceStatus : AVAILABLE");
                return "Available";
            case WifiP2pDevice.INVITED:
                //Log.d(TAG, "getDeviceStatus : INVITED");
                return "Invited";
            case WifiP2pDevice.CONNECTED:
                //Log.d(TAG, "getDeviceStatus : CONNECTED");
                return "Connected";
            case WifiP2pDevice.FAILED:
                //Log.d(TAG, "getDeviceStatus : FAILED");
                return "Failed";
            case WifiP2pDevice.UNAVAILABLE:
                //Log.d(TAG, "getDeviceStatus : UNAVAILABLE");
                return "Unavailable";
            default:
                return "Unknown = " + deviceStatus;
        }
    }
}
