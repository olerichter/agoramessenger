# AgoraMessenger

AgoraMessenger is a messenger designed for an environment without mobile networking.The messenger uses bluetooth to build a delay tolerant mesh networking.
In the network every device is working as a router and as a client for the end-to-end service.


## Usage

- you can use the AgoraMessenger.apk or build the app with AndroidStudio
- you need to make your bluetooth device discoverable indefinitely in the android settings
- the service is workung unless you force the app to stop

